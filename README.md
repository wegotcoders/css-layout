# Homepage CSS Layout #

Using CSS to design and layout a homepage.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open your local copy folder with Sublime, there is a HTML document, an images folder and a CSS style sheet.
2. Add your style rules to style.css, this file is linked to from homepage.html.
3. Open homepage.html by typing in terminal 'open homepage.html', this is a bare minimum HTML document.
4. Checkout the inspiration and useful links below, you're going to use all your CSS skills to create a homepage on a subject of your choice.
5. Add HTML elements and content to homepage.html, think about the CSS selectors you will need.
6. Add CSS rules that style the elements and content you have created, don't forget your CSS selectors.
7. Continue to add content and styling until you are happy with the result, you can see your progress by typing in terminal 'open homepage.html'.

### Inspiration Links ###

1. https://blog.hubspot.com/blog/tabid/6307/bid/34006/15-examples-of-brilliant-homepage-design.aspx
2. http://www.creativebloq.com/web-design/examples-css-912710
3. https://www.mockplus.com/blog/post/138-20-of-the-best-website-homepage-design-examples

### Useful Links ###

1. Free stock photos: https://www.pexels.com/
2. Lorem Ipsum Generator: http://generator.lorem-ipsum.info/
3. CSS Tricks: https://css-tricks.com/
